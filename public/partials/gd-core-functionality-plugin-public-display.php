<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       http://www.greendesk.pl
 * @since      1.0.0
 *
 * @package    Gd_Core_Functionality_Plugin
 * @subpackage Gd_Core_Functionality_Plugin/public/partials
 */

// Add the hamburger menu before the footer
add_action('genesis_site_title', 'add_hamburger_menu');

function add_hamburger_menu() {
    echo '<div class="hamburger-menu" id="hamburger-menu" onclick="toggleSidebar()">☰</div>';
    echo '<div class="close-menu" id="close-menu" onclick="toggleSidebar()">✖</div>';
}

add_action('after_setup_theme', 'cfp_move_secondary_sidebar_to_front');

function cfp_move_secondary_sidebar_to_front(){
    if ( 'sidebar-content-sidebar' == genesis_site_layout() ) {
        remove_action( 'genesis_after_content_sidebar_wrap', 'genesis_get_sidebar_alt' );
        add_action( 'genesis_before_content_sidebar_wrap', 'genesis_get_sidebar_alt' );
    }   
}

add_filter( 'genesis_attr_entry', 'cfp_attributes_entry' );
/**
 * Add custom data attributes to the article element in the Genesis theme.
 *
 * This function appends specific `data-` attributes to the `<article>` HTML element (`entry` in Genesis)
 * for both single posts and archive pages. The added data attributes include:
 *
 * - `data-content-name`: The title of the post.
 * - `data-content-id`: The ID of the post.
 * - `data-content-category`: The name of the post's primary category.
 * - `data-content-list-id`: (Archive pages only) The ID of the category prefixed with 'ak'.
 * - `data-content-list-name`: (Archive pages only) The name of the category prefixed with 'ak'.
 *
 * These attributes facilitate integration with Google Tag Manager (GTM) by providing structured data
 * that can be pushed to the dataLayer variable.
 *
 * @since 1.1.0
 *
 * @param array $attributes Existing attributes of the article element.
 * @return array Modified attributes including the new data attributes.
 */
function cfp_attributes_entry ( $attributes ) {
    $post_id    = get_the_ID();
    $post_title = get_the_title( $post_id );
    $categories = get_the_category( $post_id );

    $category_name = '';
    $category_id   = '';

    if ( ! empty( $categories ) ) {
        $category      = $categories[0]; // Use the primary category.
        $category_name = $category->name;
        $category_id   = $category->term_id;
    }

    // Add common data attributes.
    $attributes['data-post-name']     = esc_attr( $post_title );
    $attributes['data-post-id']       = esc_attr( $post_id );
    $attributes['data-post-category-name'] = esc_attr( $category_name );
    $attributes['data-post-category-id'] = esc_attr( $category_id );

    return $attributes;
}

add_filter( 'genesis_attr_content', 'cfp_attributes_content' );
/**
 * Add custom data attributes to the main content element in the Genesis theme.
 *
 * This function appends a `data-content-type` attribute to the `<main>` HTML element (`content` in Genesis)
 * based on the context of the current page. The `data-content-type` attribute will have the following values:
 *
 * - `'home_post_archive'` when on the home page displaying posts.
 * - `'post_archive'` when on a category archive page.
 * - `'post'` when on a single post page.
 *
 * These attributes facilitate integration with Google Tag Manager (GTM) by providing context-specific data
 * that can be pushed to the dataLayer variable.
 *
 * @since 1.1.0
 *
 * @param array $attributes Existing attributes of the main content element.
 * @return array Modified attributes including the new data attribute.
 */
function cfp_attributes_content( $attributes ) {

    $categories = get_the_category( $post_id );

    $category_name = '';
    $category_id   = '';

    if ( ! empty( $categories ) ) {
        $category      = $categories[0]; // Use the primary category.
        $category_name = $category->name;
        $category_id   = $category->term_id;
    }

    if ( is_home() ) {
        // Home page displaying posts.
        $attributes['data-content-type'] = 'home_post_archive';
    } elseif ( is_category() ) {
        // Category archive page.
        $attributes['data-content-type'] = 'post_archive';
        $attributes['data-post-list-id'] = esc_attr( $category_id );
        $attributes['data-post-list-name'] = esc_attr( $category_name );
    } elseif ( is_singular( 'post' ) ) {
        // Single post page.
        $attributes['data-content-type'] = 'post';
    }
    return $attributes;
}