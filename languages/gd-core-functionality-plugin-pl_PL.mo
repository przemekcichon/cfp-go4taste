��            )   �      �  #   �  *   �     �     �  	                  )  
   1     <     M     T     m     �     �     �     �     �     �     �  	   �  
   �  
   �       	   
          ,  	   8     B  �  J     :  *   Z     �     �     �     �     �  
   �     �     �     �  (        1     I  	   f     p     }     �     �     �     �     �     �     �  	   �     	     	     0	  	   >	                    
                                                      	                                                               %2$s restored to revision from %1$s %2$s scheduled for: <strong>%1$s</strong>. %s %s draft updated. %s saved. %s submitted. %s updated. Add New Add New %s Add or remove %s All %s Choose from most used %s Custom field deleted. Custom field updated. Edit %s M j, Y @ G:i New %s New %s Name No %s found No %s found in Trash Parent %s Parent %s: Popular %s Program Search %s Seperate %s with commas Show all %s Update %s View %s Project-Id-Version: GD Core Functionality Plugin
POT-Creation-Date: 2017-04-25 07:55+0200
PO-Revision-Date: 2017-04-25 10:16+0200
Last-Translator: 
Language-Team: 
Language: pl_PL
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.12
X-Poedit-Basepath: ..
X-Poedit-WPHeader: gd-core-functionality-plugin.php
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
 %2$s przywrócony do wersji%1$s %2$s zaplanowane <strong>na:%1$s.</strong> %s %s szkic zaktualizowany. %s zapisano. %s wysłany. %s zaktualizowano. Dodaj nowy Dodaj nowy %s Dodaj albo usuń %s Wszystkie %s Wybierz spośród najpopularniejszych %s Pole własne usunięte. Pole własne zaktualizowane. Edytuj %s M j, Y @ G:i Nowy %s Dodaj nowy %s Nie znaleziono żadnych %s Nie znaleziono %s w koszu Nadrzędne %s Nadrzędny %s: Popularne %s Program Szukaj %s Rozdziel %s przecinkami Pokaż wszystkie %s Aktualizuj %s Zobacz %s 