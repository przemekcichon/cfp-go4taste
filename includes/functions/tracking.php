<?php 


// Add Google Tag Manager code in <head>
add_action( 'wp_head', function (){ ?>
	<!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-PQ2MQC2');</script>
    <!-- End Google Tag Manager -->
<?php }, 20 );

// Add Google Tag Manager code immediately below opening <body> tag
add_action( 'genesis_before', function () { ?>
	<!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PQ2MQC2"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
<?php }, 10 );

add_action( 'wp_head', 'cfp_push_page_type_to_datalayer', 10 );
/**
 * Push page type data to the GTM dataLayer based on the current page context.
 *
 * This function determines the page type and post type (if applicable) and
 * pushes this information to the GTM dataLayer variable. The pushed data includes:
 *
 * - 'page_type': The type of the current page, formatted as '{{post_type}}_archive_{{archive type suffix}}'.
 * - 'post_type': The post type of the current post or archive.
 *
 * @since 1.1.0
 */
function cfp_push_page_type_to_datalayer() {
    // Initialize variables.
    $page_type = '';
    $post_type = '';

    if ( is_singular() ) {
        // Single post or page.
        global $post;
        $post_type = get_post_type( $post );
        $page_type = $post_type;
    } elseif ( is_home() && 'posts' === get_option( 'show_on_front' ) ) {
        // Blog home page displaying latest posts.
        $post_type = 'post';
        $page_type = 'home';
    } elseif ( is_archive() ) {
        // Archive pages.
        $archive_type_suffix = '';

        if ( is_post_type_archive() ) {
            // Post type archive.
            $post_type = get_query_var( 'post_type' );
            if ( empty( $post_type ) ) {
                $post_type = 'post';
            } elseif ( is_array( $post_type ) ) {
                // If multiple post types, join them with an underscore.
                $post_type = implode( '_', $post_type );
            }
            $archive_type_suffix = 'post_type_archive';
        } else {
            // For other archive types, default post_type to 'post' unless determined otherwise.
            $post_type = 'post';

            if ( is_category() ) {
                $archive_type_suffix = 'category';
            } elseif ( is_tag() ) {
                $archive_type_suffix = 'tag';
            } elseif ( is_author() ) {
                $archive_type_suffix = 'author';
            } elseif ( is_date() ) {
                $archive_type_suffix = 'date';
            } elseif ( is_tax() ) {
                // Custom taxonomy.
                $taxonomy = get_queried_object();
                $archive_type_suffix = 'taxonomy';

                // Attempt to determine post type associated with the taxonomy.
                $tax_post_types = get_taxonomy( $taxonomy->taxonomy )->object_type;
                if ( ! empty( $tax_post_types ) ) {
                    $post_type = implode( '_', $tax_post_types );
                }
            } else {
                // Other archives.
                $archive_type_suffix = 'archive';
            }
        }

        $page_type = $post_type . '_archive_' . $archive_type_suffix;
    } elseif ( is_front_page() ) {
        // Static front page.
        $post_type = 'page';
        $page_type = 'home';
    } else {
        // Other types of pages (e.g., search results, 404).
        $page_type = 'other';
        $post_type = '';
    }

    // Output the dataLayer push script.
    ?>
    <script>
    window.dataLayer = window.dataLayer || [];
    dataLayer.push({
        'event': 'pageTypeDetected',
        'page_type': '<?php echo esc_js( $page_type ); ?>',
        'post_type': '<?php echo esc_js( $post_type ); ?>'
    });
    </script>
    <?php
}
