<?php

add_action( 'after_setup_theme', function(){
    remove_theme_support( 'custom-header' );
    $params = array(
        'width'         => 289,
        'height'        => 52,
        'flex-width'    => true,
        'flex-height'   => true,
        'header-text' => '',
    );
    add_theme_support( 'custom-logo', $params );
    
} );

add_filter( 'genesis_seo_title', 'cfp_navbar_brand' );
/**
 * Echo the navbar-brand menu.
 *
 * @since 0.1.0
 */
function cfp_navbar_brand( $title ){
    
	$title = sprintf( '<p %s>%s</p>', genesis_attr( 'site-title' ), get_custom_logo() );
    return $title;
        
}