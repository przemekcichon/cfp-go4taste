<?php 

add_filter( 'jetpack_photon_pre_image_url', function( $image_url, $args, $scheme ){
	$image_url = preg_replace('/loc.go4taste.pl/', 'www.go4taste.pl/blog', $image_url);
	return $image_url;
}, 10, 3 );
